<?php

namespace NoiLib;
class Core extends Slim\App{
    public function start(){
    $this->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});
     $this->run();    
    }
}
